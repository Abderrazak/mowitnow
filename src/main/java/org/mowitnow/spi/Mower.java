package org.mowitnow.spi;

import org.mowitnow.engine.MowerState;

/**
 * <p>This class is aimed to handle all what a Mower in our system is supposed able to do</p>
 */
public interface Mower {

    /**
     * @param command
     */
    void process(MowerCommand command);

    /**
     * @return
     */
    MowerState readState();

    String sayHello();
}
