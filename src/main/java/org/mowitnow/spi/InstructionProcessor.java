package org.mowitnow.spi;

import org.mowitnow.engine.LawnGrid;
import org.mowitnow.engine.MowerInstruction;

public interface InstructionProcessor {
    LawnGrid buildGrid();

    boolean hasMowers();

    MowerInstruction next();
}
