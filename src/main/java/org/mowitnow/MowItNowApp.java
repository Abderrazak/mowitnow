package org.mowitnow;

import org.mowitnow.engine.*;
import org.mowitnow.spi.InstructionProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 */
public class MowItNowApp {

    private static final Logger logger = LoggerFactory.getLogger(MowItNowApp.class);

    private static void go(String arg) {
        InstructionProcessor instructionProcessor = new FileInstructionProcessor(arg);
        LawnGrid lawnGrid = instructionProcessor.buildGrid();
        while (instructionProcessor.hasMowers()) {
            MowerInstruction next = instructionProcessor.next();
            SimpleMower mower = new SimpleMower(lawnGrid.getWidth(), lawnGrid.getHeight(), next.getState());
            next.getCommands().forEach(mower::process);
            logger.info(mower.readState().toString());
        }
    }


    public static final void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Please indicate an instruction file");
        try {
            logger.info("Start ");
            go(args[0]);
        } catch (Exception e) {
            logger.error("Exited with errors", e);
            System.exit(1);
        }
        logger.info("Exited with no errors");
        System.exit(0);
    }
}
