package org.mowitnow.engine;

import org.apache.commons.lang3.StringUtils;
import org.mowitnow.spi.InstructionProcessor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class FileInstructionProcessor implements InstructionProcessor {

    private static final String REGEX_WHITESAPCE = " ";
    private static final String PATTERN_EOL_LINUX = "\n";
    private static final String FILE_ENCODING = "UTF-8";
    private Scanner instructionFileScanner = null;
    private static LawnGrid grid = null;

    public FileInstructionProcessor(String instructionPathFilename) {
        try {
            instructionFileScanner = new Scanner(new FileInputStream(instructionPathFilename), FILE_ENCODING).useDelimiter(PATTERN_EOL_LINUX);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("instruction file not found");
        }
    }

    @Override
    public LawnGrid buildGrid() {
        if (grid == null) {
            final String gridLine = instructionFileScanner.nextLine();
            final String[] split = gridLine.split(REGEX_WHITESAPCE);
            grid = new LawnGrid(Integer.decode(split[0]), Integer.decode(split[1]));
        }
        return grid;
    }

    @Override
    public boolean hasMowers() {
        return instructionFileScanner.hasNext();
    }

    @Override
    public MowerInstruction next() {
        while (instructionFileScanner.hasNext()) {
            if (grid == null)
                buildGrid();
            else {
                final String stateString = instructionFileScanner.nextLine();
                if (StringUtils.isEmpty(stateString))
                    throw new IllegalArgumentException("Data within instruction file is wrong");
                final StringTokenizer mowerStateTokenizer = new StringTokenizer(stateString, " ");
                if (mowerStateTokenizer.countTokens() != 3)
                    throw new IllegalArgumentException("Data error : number of arguments to initialize a Mower state incorrect");
                final String sequenceString = instructionFileScanner.nextLine();
                if (StringUtils.isEmpty(sequenceString))
                    throw new IllegalArgumentException("Data within instruction file is wrong");
                MowerInstruction mowerInstruction = MowerInstruction.getInstance();
                mowerInstruction.setState(MowerState.build(mowerStateTokenizer));
                // TODO use lambda
                final char[] seq = sequenceString.toCharArray();
                for (int i = 0; i < seq.length; i++) {
                    mowerInstruction.addCommand(String.valueOf(seq[i]));
                }
                return mowerInstruction;
            }
        }
        return null;
    }
}
