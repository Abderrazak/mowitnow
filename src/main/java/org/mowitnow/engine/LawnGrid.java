package org.mowitnow.engine;

/**
 * <p>A simple abstraction of a Lawn Grid which may help when dealing with multiple grids</p>
 */
public class LawnGrid {
    private final int width;
    private final int height;

    public LawnGrid(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LawnGrid)) return false;

        LawnGrid lawnGrid = (LawnGrid) o;

        if (height != lawnGrid.height) return false;
        if (width != lawnGrid.width) return false;

        return true;
    }
}
