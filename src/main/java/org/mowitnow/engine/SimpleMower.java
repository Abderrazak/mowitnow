package org.mowitnow.engine;

import org.mowitnow.spi.Mower;
import org.mowitnow.spi.MowerCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SimpleMower implements Mower {

    private static final Logger logger = LoggerFactory.getLogger(SimpleMower.class);

    private static final String NORTH = "N";
    private static final String WEST = "W";
    private static final String SOUTH = "S";
    private static final String EAST = "E";
    private static final String LEFT = "G";
    private static final String RIGHT = "D";
    private static final String FORWARD = "A";
    private int gridWidth;
    private int gridHeight;
    private String direction;
    private int y;
    private int x;
    private final List<String> allowedDirections = Arrays.asList(NORTH, EAST, WEST, SOUTH);

    /**
     * a utility mapper that helps to predicate targeted direction from the actual one when mower turns left
     */
    private static final Map<String, String> goLeftMapper = new HashMap<String, String>() {{
        put(NORTH, WEST);
        put(WEST, SOUTH);
        put(SOUTH, EAST);
        put(EAST, NORTH);
    }};

    /**
     * a utility mapper that helps to predicate targeted direction from the actual one when mower turns right
     */
    private static final Map<String, String> goRightMapper = new HashMap<String, String>() {{
        put(NORTH, EAST);
        put(EAST, SOUTH);
        put(SOUTH, WEST);
        put(WEST, NORTH);
    }};

    // TODO should be injected
    private static final Map<String, String> commandMapper = new HashMap<String, String>() {{
        put("org.mowitnow.engine.GoRightCommand", RIGHT);
        put("org.mowitnow.engine.GoLeftCommand", LEFT);
        put("org.mowitnow.engine.GoForwardCommand", FORWARD);
    }};

    public SimpleMower(int gridWidth, int gridHeight, MowerState state) {
        initGridSize(gridWidth, gridHeight);
        initState(state);
    }

    private void initState(MowerState state) {
        final int targetX = state.getXpos();
        final int targetY = state.getYpos();
        final String targetDirection = state.getDirection().name();
        if (targetX < 0 || targetX > this.gridWidth || targetY < 0 || targetY > this.gridHeight) {
            throw new IllegalStateException("Mower coordinates are wrong.");
        }
        this.x = targetX;
        this.y = targetY;
        if (targetDirection == null || !allowedDirections.contains(targetDirection))
            throw new IllegalStateException("Allowed directions are N,E,W,S");
        this.direction = targetDirection;
    }

    private void initGridSize(int gridWidth, int gridHeight) {
        if (gridWidth < 0 || gridHeight < 0)
            throw new IllegalStateException("Grid size is wrong.");
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
    }


    public void process(MowerCommand command) {
        String cmd = commandMapper.get(command.getClass().getName());
        switch (cmd) {
            case LEFT:
                goLeft();
                break;
            case RIGHT:
                goRight();
                break;
            case FORWARD:
                goForward();
                break;
        }
    }

    public MowerState readState() {
        return MowerState.buildWithCoordinates(this.x, this.y, MowerState.Direction.valueOf(this.direction));
    }

    @Override
    public String sayHello() {
        return null;
    }

    private void goForward() {
        switch (direction) {
            case NORTH:
                if (y + 1 <= gridHeight)
                    incY();
                break;
            case SOUTH:
                if (y - 1 >= 0)
                    decY();
                break;
            case EAST:
                if (x + 1 <= gridWidth)
                    incX();
                break;
            case WEST:
                if (x - 1 >= 0)
                    decX();
                break;
        }
        if (logger.isDebugEnabled())
            logger.debug("forward");
    }

    private void goRight() {
        this.direction = goRightMapper.get(direction);
        if (logger.isDebugEnabled())
            logger.debug("turn left direction set to " + this.direction);
    }

    private void goLeft() {
        this.direction = goLeftMapper.get(direction);
        if (logger.isDebugEnabled())
            logger.debug("turn right set to " + this.direction);
    }

    private void incY() {
        this.y++;
    }

    private void decY() {
        this.y--;
    }

    private void incX() {
        this.x++;
    }

    private void decX() {
        this.x--;
    }
}
