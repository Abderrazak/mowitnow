package org.mowitnow.engine;

import org.mowitnow.spi.MowerCommand;

public class GoRightCommand implements MowerCommand {
    @Override
    public boolean equals(Object obj) {
        return obj instanceof GoRightCommand;
    }
}
