package org.mowitnow.engine;

import org.mowitnow.spi.MowerCommand;

public class GoForwardCommand implements MowerCommand {

    @Override
    public boolean equals(Object obj) {
        return obj instanceof GoForwardCommand;
    }
}
