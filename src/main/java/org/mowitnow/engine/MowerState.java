package org.mowitnow.engine;

import java.util.StringTokenizer;

/**
 * a simple immutable class for <code>Mower</code> state transport.
 */
public final class MowerState {

    private static final String WHITE_SPACE = " ";
    private final int xpos;
    private final int ypos;
    private final Direction direction;

    public static MowerState build(StringTokenizer tokenizer) {
        return new MowerState(Integer.decode(tokenizer.nextToken()), Integer.decode(tokenizer.nextToken()), MowerState.Direction.valueOf(tokenizer.nextToken()));
    }

    public static MowerState buildWithCoordinates(int xpos, int ypos, Direction direction) {
        return new MowerState(xpos,ypos,direction);
    }
    private MowerState(int xpos, int ypos, Direction direction) {
        this.xpos = xpos;
        this.ypos = ypos;
        this.direction = direction;
    }

    public int getXpos() {
        return xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public Direction getDirection() {
        return direction;
    }

    enum Direction {
        N, E, W, S;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MowerState)) return false;

        MowerState that = (MowerState) o;

        if (xpos != that.xpos) return false;
        if (ypos != that.ypos) return false;
        if (direction != that.direction) return false;

        return true;
    }

    @Override
    public String toString() {
        return xpos + WHITE_SPACE + ypos + WHITE_SPACE + direction;
    }
}
