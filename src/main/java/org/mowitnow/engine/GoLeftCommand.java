package org.mowitnow.engine;

import org.mowitnow.spi.MowerCommand;

public class GoLeftCommand implements MowerCommand {
    @Override
    public boolean equals(Object obj) {
        return obj instanceof GoLeftCommand;
    }
}
