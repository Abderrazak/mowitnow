package org.mowitnow.engine;

import org.mowitnow.spi.MowerCommand;

import java.util.ArrayList;
import java.util.List;

public final class MowerInstruction {
    private MowerState state;
    private List<MowerCommand> commands;

    public List<MowerCommand> getCommands() {
        return commands;
    }

    public MowerState getState() {
        return state;
    }

    public void setState(MowerState state) {
        this.state = state;
    }

    public void addCommand(String command) {
        if (commands == null)
            commands = new ArrayList<>();
        switch (command) {
            case "A":
                commands.add(new GoForwardCommand());
                break;
            case "D":
                commands.add(new GoRightCommand());
                break;
            case "G":
                commands.add(new GoLeftCommand());
                break;
        }
    }

    private MowerInstruction() {
    }

    public static MowerInstruction getInstance() {
        return new MowerInstruction();
    }
}
