package org.mowitnow.engine;

import org.apache.commons.io.IOUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mowitnow.spi.InstructionProcessor;
import org.mowitnow.spi.MowerCommand;

import java.io.*;
import java.util.List;

public class InstructionProcessorTest {

    private final String ROOT_WORKING_DIRECTORY = System.getProperty("java.io.tmpdir");
    private String relativeWorkingFolder;

    @Before
    public void before() throws IOException {
        relativeWorkingFolder = System.currentTimeMillis() + File.separator + "mower";
        final boolean mkdirs = new File(ROOT_WORKING_DIRECTORY + File.separator + relativeWorkingFolder).mkdirs();
        if (!mkdirs)
            throw new IllegalStateException("Creation of temporary working folders failed");
        final String fixtureFileName = "valid_one_mower.dat";
        final FileOutputStream outputStream = new FileOutputStream(buildFictureFilePath(fixtureFileName));
        final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fixtureFileName);
        IOUtils.copy(inputStream, outputStream);
    }

    private String buildFictureFilePath(String fixtureFileName) {
        return ROOT_WORKING_DIRECTORY + File.separator + relativeWorkingFolder + File.separator + fixtureFileName;
    }

    @Test
    public void fileInstructionProcessorShouldBeAbleToBuildGridObject() {
        final String fixtureFileName = "valid_one_mower.dat";
        InstructionProcessor instructionProcessor = new FileInstructionProcessor(buildFictureFilePath(fixtureFileName));
        LawnGrid lawnGrid = instructionProcessor.buildGrid();
        Assertions.assertThat(lawnGrid).isNotNull().isEqualTo(new LawnGrid(5, 5));
    }

    @Test
    public void fileInstructionProcessorShouldLoadInstructionFileAndBuildCommandSequence() {
        final String fixtureFileName = "valid_one_mower.dat";
        InstructionProcessor instructionProcessor = new FileInstructionProcessor(buildFictureFilePath(fixtureFileName));
        if (instructionProcessor.hasMowers()) {
            MowerInstruction mowerInstruction = instructionProcessor.next();
            Assertions.assertThat(mowerInstruction.getState()).isNotNull().isEqualTo(MowerState.buildWithCoordinates(0, 0, MowerState.Direction.N));
            List<MowerCommand> commands = mowerInstruction.getCommands();
            Assertions.assertThat(commands).isNotEmpty().hasSize(1).containsSequence(new GoForwardCommand());
        }
    }
}
