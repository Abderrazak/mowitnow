package org.mowitnow.engine;

import org.fest.assertions.Assertions;
import org.junit.Test;

public class MowerTest {

    private final int gridWidth = 5;
    private final int gridHeight = 5;

    @Test
    public void shouldIndicateEastFromNorthAfterTurnRight() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.N));
        mower.process(new GoRightCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 E");
    }

    @Test
    public void shouldIndicateSouthFromEastAfterTurnRight() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.E));
        mower.process(new GoRightCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 S");
    }

    @Test
    public void shouldIndicateWestFromSouthAfterTurnRight() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.S));
        mower.process(new GoRightCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 W");
    }

    @Test
    public void shouldIndicateNorthFromWestAfterTurnRight() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.W));
        mower.process(new GoRightCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 N");
    }

    @Test
    public void shouldIndicateWestFromNorthAfterTurnLeft() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.N));
        mower.process(new GoLeftCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 W");
    }

    @Test
    public void shouldIndicateSouthFromWestAfterTurnLeft() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.W));
        mower.process(new GoLeftCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 S");
    }

    @Test
    public void shouldIndicateEastFromSouthAfterTurnLeft() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.S));
        mower.process(new GoLeftCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 E");
    }

    @Test
    public void shouldIndicateNorthFromEastAfterTurnLeft() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.E));
        mower.process(new GoLeftCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 N");
    }

    @Test
    public void forwardToNorthShouldIncrementYAxis() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.N));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 1 N");
    }

    @Test
    public void forwardToSouthShouldDecrementYAxis() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 1, MowerState.Direction.S));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 S");
    }

    @Test
    public void forwardToEastShouldIncrementXAxis() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.E));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("1 0 E");
    }

    @Test
    public void forwardToWestShouldDecrementXAxis() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(1, 0, MowerState.Direction.W));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 W");
    }

    @Test
    public void axisXShouldNotChangeWhenOutOfGrid() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.W));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 W");
    }

    @Test
    public void axisYShouldNotChangeWhenOutOfGrid() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(0, 0, MowerState.Direction.S));
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("0 0 S");
    }

    @Test
    public void executionOfSequenceShouldLeadsToAnIndicatedPosition() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(1, 2, MowerState.Direction.N));
        mower.process(new GoLeftCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoLeftCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoLeftCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoLeftCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("1 3 N");
    }

    @Test
    public void executionOfSequenceShouldLeadsToAnIndicatedPosition2() {
        SimpleMower mower = new SimpleMower(gridWidth, gridHeight, MowerState.buildWithCoordinates(3, 3, MowerState.Direction.E));
        mower.process(new GoForwardCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoRightCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoRightCommand());
        mower.process(new GoForwardCommand());
        mower.process(new GoRightCommand());
        mower.process(new GoRightCommand());
        mower.process(new GoForwardCommand());
        Assertions.assertThat(mower.readState().toString()).isNotNull().isEqualTo("5 1 E");
    }
}
