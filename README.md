# Mow It Project 2 #

to execute do the following :
java 8 upgraded

```
#!linux

git clone git@bitbucket.org:Abderrazak/mowitnow.git
./build.sh
./run.sh
```
**you may expecte the following output after execution** :

```
#!console

[main] INFO org.mowitnow.MowItNowApp - Start 
[main] INFO org.mowitnow.MowItNowApp - 1 3 N
[main] INFO org.mowitnow.MowItNowApp - 5 1 E
[main] INFO org.mowitnow.MowItNowApp - Exited with no errors
```

you need :
Java 7+
Maven 3+
A Shell :)
